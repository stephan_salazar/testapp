/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var bcrypt = require('bcrypt');

module.exports = {

  attributes: {

    email:{
      type: 'string',
      required: true,
      unique: true,
      email: true,
      primaryKey: true
    },

    password:{
      type: 'string',
      required: true,
    },

    posts: {
      collection: 'post',
      via: 'owner'
    }

  },

  beforeCreate: function (values, cb) {
    bcrypt.hash(values.password, 10, function(err, hash){
      if(err) return cb(err);
      values.password = hash;
      cb();
    })
  },

  beforeDestroy: function(criteria, cb) {

    post = Post
    .findAll({where:{owner:criteria.where.email}})
    //.destroy()
    ;

    console.log(post);

    cb();

  }

};
